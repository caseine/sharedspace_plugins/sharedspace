<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of useful functions
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package local_sharedspace
 */
// moodleform is defined in formslib.php
//
//
require_once("$CFG->libdir/formslib.php");

/**
 *
 *
 * The form that is the root of the shared space
 **/
class spaceform extends moodleform {
    // Add elements to form
    public function definition() {
        global $CFG, $DB;

        $mform = $this->_form; // Don't forget the underscore!

        /*$mform->addElement('text', 'email', get_string('email')); // Add elements to your form
        $mform->setType('email', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('email', 'Please enter email pour les zozos');        //Default value

        $mform->addElement('button', 'intro', "CLIQUEZ POUR DETRUIRE LES ZOZOS");

        $mform->addElement('static', 'description', "Test1 ",'Contenu de $CFG->wwwroot : '.$CFG->wwwroot);

        $mform->addElement('static', 'description', "Test2 ",'Contenu de $CFG->dirroot : '.$CFG->dirroot);

        $mform->addElement('static', 'description', "Test3 ",'Contenu de $CFG->libdir  : '.$CFG->libdir);

        $mform->addElement('filepicker', 'attachment', 'VOIR ?',null,null);*/

        // A) Saisir le fichier .csv pour re-tagger un ensemble d'activités
        $mform->addElement('header','Parsing and tagging from .csv','Parsing and tagging from .csv');
        $maxbytes = 8024;
        $mform->addElement('filepicker', 'csv_auto_tag_file', get_string('file'), null,
                     array('maxbytes' => $maxbytes, 'accepted_types' => '.csv'));
        $this->add_action_buttons(false, 'Parse and load');
        // $mform->addElement('header','Parsing and tagging from .csv','Parsing and tagging from .csv');
        // $maxbytes = 8024;
        // $mform->addElement('filepicker', 'csv_auto_tag_file', get_string('file'), null,
        //              array('maxbytes' => $maxbytes, 'accepted_types' => '.csv'));
        // $encodings = core_text::get_encodings();
        // $mform->addElement('select', 'encoding', 'Encodage', $encodings);

        // $radio = array();
        // $radio[] = $mform->createElement('radio', 'separator', null, 'tab', 'tab');
        // $radio[] = $mform->createElement('radio', 'separator', null, 'comma', 'comma');
        // $radio[] = $mform->createElement('radio', 'separator', null, 'colon', 'colon');
        // $radio[] = $mform->createElement('radio', 'separator', null, 'semicolon', 'semicolon');
        // $mform->addGroup($radio, 'separator', 'Separateur', ' ', false);
        // $mform->setDefault('separator', 'comma');
        // $this->add_action_buttons(false, 'Parse and load');

        // B) Telecharger un fichier .csv contenant tous les modules d'un cours
       //B) Telecharger un fichier .csv contenant tous les modules d'un cours
       $mform->addElement('header','Download all modules id from a course','Download all modules id from a course');
       $maxbytes = 8024;
       $data_courses = array_keys($DB->get_records_sql("SELECT DISTINCT `fullname` FROM `course`"));
       $mform->addElement('select', 'course_selection', 'Courses', $data_courses);
       $this->add_action_buttons(false, 'Download from course');

        // B) Telecharger un fichier .csv contenant tous les modules d'une catégorie de cours
        //  $mform->addElement('header','Download all modules id from a course category','Download all modules id from a course category');
        //  $data_courses_cat = array_keys($DB->get_records_sql("SELECT DISTINCT `name` FROM `course_categories`"));
        //  $mform->addElement('select', 'course_cat_selection', 'Courses categories', $data_courses_cat);
        //  $this->add_action_buttons(false, 'Download from category');

        // echo "------------------------------------------------------------------------<br/>";
        // Show all fields.
        $categories = $DB->get_records('local_metadata_category', ['contextlevel' => CONTEXT_MODULE], 'sortorder ASC');
        foreach($categories as $category) {
            $mform->addElement('header',$category->name, $category->name);
            $fields = $DB->get_records('local_metadata_field', ['contextlevel' => CONTEXT_MODULE, 'categoryid' => $category->id], 'sortorder ASC');
            foreach($fields as $field){
                $sql = "SELECT DISTINCT `data` FROM `local_metadata` WHERE `fieldid` = ".$field->id;
                $dataField = $DB->get_records_sql($sql);
                $propositions = array_column($dataField, 'data');
                $options = array(
                    'multiple' => true,
                    // 'noselectionstring' => $field->name,
                );
                $mform->addElement('autocomplete', $field->id, $field->name, $propositions, $options);
            }
        }
        $this->add_action_buttons(false, get_string('search', 'search'));

        // Show the file parsing field
        // Hadrien: try to insert html by stopping the php section and re-opening it aftewards.
        /*?>
        <form method="post" action="cibleparsing.php" enctype="multipart/form-data">
            <br/>
            <p> ------------------------------------------------------------------------ </p>
            <p> Saisir le fichier .csv pour re-tagger un ensemble d'activités </p>
            <input type="file" name="fichier_activitees_tagges" /><br />
            <input type="submit" value="Valider" />
            <p> ------------------------------------------------------------------------ </p>
            <br/>
        </form>

        <?php*/

    }
    // Custom validation should be added here
    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }
}

