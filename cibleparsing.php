<?php
// This file is part of Caseine shared space and Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// Page printing the result of parsing the .csv file  

//echo $OUTPUT->header();
//echo $OUTPUT->custom_block_region('content');

echo "<br/>"; 
echo  "  PARSING DU FICHIER .csv <br/>";
echo "<br/>";
//phpinfo();

echo "code error: " . $_FILES['fichier_activitees_tagges']['error'] . "<br/>";
$pathfichier = 'none';

//A) Receive the upload
//Check that the file is uploaded without troubles
//Store it on server and record its path in $pathfichier
if (isset($_FILES['fichier_activitees_tagges']) && 
         $_FILES['fichier_activitees_tagges']['error'] == 0) {

            $taille_en_octets = $_FILES['fichier_activitees_tagges']['size'];
            echo "Nom du fichier         : " . $_FILES['fichier_activitees_tagges']['name'] . "<br/>";
            echo "Taille du fichier      : " . $taille_en_octets . "<br/>";
            echo "Type du fichier        : " . $_FILES['fichier_activitees_tagges']['type'] . "<br/>";
            echo "Repertoire temporaire  : " . $_FILES['fichier_activitees_tagges']['tmp_name'] . "<br/>";
            $taille_max = 10000000;
            if ($taille_en_octets <= $taille_max) { //moins de 10 Mo
                $infosfichier = pathinfo($_FILES['fichier_activitees_tagges']['name']);
                $extension_upload = $infosfichier['extension'];
                if ($extension_upload == 'csv') {
                    $nom_fichier = basename($_FILES['fichier_activitees_tagges']['name']);
                    $pathfichier = '/var/www/moodle/local/sharedspace/uploads/' . $nom_fichier;
                    move_uploaded_file($_FILES['fichier_activitees_tagges']['tmp_name'], $pathfichier);
                    echo 'fichier stocké dans : ' . $pathfichier . "<br/>";;
                }
            } else {
                echo "la taille maximum pour ce .csv est de " . $taille_max . "<br/>";
            }    
}

//B) Parse the file received
if ($pathfichier != 'none') { //If the file has been stored safely, its path on the server is known
    global $DB;
    echo "<br/>";
    echo "  Test de parsing d'un fichier <br/>";
    echo "<br/>";
    
    //Hadrien: pour resoudre un pb d'encoding des fins de lignes sous MAC: (... argghhh ...):
    //         en a-t-on besoin sur andromede ?
    ini_set("auto_detect_line_endings", true);

    //Open the file in read-only mode
    $fichier_activites = fopen($pathfichier, 'r');

    //Parse the first line (allow the first line to have a max size of 8ko, I think...)
    $ligne = fgets($fichier_activites, 8024);   
    $field_names = explode(';',$ligne); //Extract the names of the fields as an array
    
    while (!feof($fichier_activites)) {
        //Parse the next line
        $ligne = fgets($fichier_activites, 8024);
        
        echo $ligne . "<br/>";
        $field_values = explode(';',$ligne); //Extract the values as an array
        for ($i = 0; $i < count($field_values); $i++) {
            $value_i = $field_values[$i];
            echo "set " . $field_names[$i] . " = " . $value_i . "<br/>";
        }
        print_r($field_values);
        echo "<br/>";
        $sql = "SELECT * FROM `course_modules` WHERE `id`='".$field_values[0]."'";
        echo $sql . "<br/>";
        $instancesid = $DB->get_records_sql($sql);
        print_r($instancesid);     
        echo "<br/>";
    }
    //Ferme le fichier
    fclose($fichier_activites);
}

//echo $OUTPUT->footer();
?>