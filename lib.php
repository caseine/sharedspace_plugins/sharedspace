<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of useful functions
 *
 * @copyright 1999 Martin Dougiamas  http://dougiamas.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package local_sharedspace
 * */
defined('MOODLE_INTERNAL') || die;



function local_sharedspace_extend_settings_navigation($settingsnav, $context) {
    global $CFG, $PAGE;

    // Only add this settings item on non-site course pages.
    /*if (!$PAGE->course or $PAGE->course->id == 1) {
        return;
    }

    // Only let users with the appropriate capability see this settings item.
    if (!has_capability('moodle/backup:backupcourse', context_course::instance($PAGE->course->id))) {
        return;
    }


    if ($settingnode = $settingsnav->find('courseadmin', navigation_node::TYPE_COURSE)) {
        $strfoo = get_string('index', 'local_sharedspace');
        $url = new moodle_url('/local/sharedspace/index.php', array('id' => $PAGE->course->id));
        $foonode = navigation_node::create(
            $strfoo,
            $url,
            navigation_node::NODETYPE_LEAF,
            'sharedspace',
            'sharedspace',
            new pix_icon('t/addcontact', $strfoo)
        if ($PAGE->url->compare($url, URL_MATCH_BASE)) {
            $foonode->make_active();
        }
        $settingnode->add_node($foonode);
    }*/

}

function local_sharedspace_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    if ($context->contextlevel != CONTEXT_SYSTEM) {
        return false;
    }

    // Make sure the filearea is one of those used by the plugin.
    if ($filearea !== 'sharedspace') {
        return false;
    }

    // Make sure the user is logged in and has access to the module (plugins that are not course modules should leave out the 'cm' part).
    require_login($course, true);

    // Check the relevant capabilities - these may vary depending on the filearea being accessed.
    if (!has_capability('local/sharedspace:accesstospace', $context)) {
        return false;
    }

    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_sharedspace', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    // echo "moodle file in plugin function: " . $file->get_content();
    // We can now send the file back to the browser - in this case with a cache lifetime of 1 day and no filtering.
    // From Moodle 2.3, use send_stored_file instead.
    send_stored_file($file, 86400, 0, $forcedownload, $options);
}

// M2.9
function local_sharedspace_extend_navigation(global_navigation $nav) {
    global $PAGE;
    static $jsInserted = false;
    $nav->add(get_string('linkname', 'local_extsearch'), new moodle_url('/local/extsearch/index.php'));
    if (!$jsInserted) {
        $PAGE->requires->js_init_code('
window.toggleDetails = function (n) {
    if (!/\btoggle-details\b/.exec(n.className)) {
        return true;
    }
    var details = n.getElementsByTagName("div")[0];
    var disp = details.style.display;
    details.style.display = (disp == "block" ? "none" : "block");
    return false;
}
');
        $jsInserted = true;
    }
}

function get_autocomplete_data($fromform) {
    global $DB;
    // $result = new stdClass;

    $fields = $DB->get_records('local_metadata_field', ['contextlevel' => CONTEXT_MODULE], 'sortorder ASC');
    foreach($fromform as $key => $value) {
        if (is_int($key)) {
            // $result->$key = array();
            // $dataField = $DB->get_records('local_metadata', ['fieldid' => $key]);
            $sql = "SELECT DISTINCT `data` FROM `local_metadata` WHERE `fieldid` = ".$key;
            $dataField = $DB->get_records_sql($sql);
            $keys = array_keys($dataField);
            foreach($value as $choice) {
                $result->$key[] = $dataField[$keys[intval($choice)]]->data;

                // print($dataField[$keys[intval($choice)]]->data);
            }
        }
    }

    if (!isset($result)) {
        return null;
    }
    // print_r($result);
    return $result;
}

function simple_request($criteria) {
    global $DB;
    // print_r($criteria);
    $data_criteria = get_autocomplete_data($criteria);
    if (is_null($data_criteria)) { return null;
    }
    $sql = "SELECT DISTINCT instanceid FROM `local_metadata` WHERE ";
    foreach($data_criteria as $key => $array_value) {
        // $instancesid = $DB->get_records('local_metadata', array('fieldid' => $key, $DB->sql_compare_text('data') => $DB->sql_compare_text($array_value[0])));
        $sql .= "(`fieldid` = " . "'".$key."' AND (";
        foreach($array_value as $value) {
            $sql .= "`data` LIKE '".$value."' OR ";
        }
        $sql = substr($sql, 0, -4); // remove the last " OR "
        $sql .= ")) AND ";
    }
    $sql = substr($sql, 0, -5); // remove the last " AND "
    // print($sql);

    // $instancesid = $DB->get_records_select('local_metadata', $where);
    $instancesid = $DB->get_records_sql($sql);
    $results = array();
    foreach($instancesid as $instanceid) {
        $cm = get_course_and_cm_from_cmid($instanceid->instanceid);
        // add to results only if the module belong to a lab
        $id_cat_lab = get_id_cat_lab();
        if ($cm[0]->category == $id_cat_lab) {
            $results[] = $cm[1];
        }
        // PRINT
        /*$cms = $DB->get_records('course_modules', ['id' => $instanceid->instanceid]);
        foreach($cms as $module) {
            print($module->course);
        }*/
    }
    // print_r($results);
    return $results;
}

function print_module($cm) {
    global $CFG, $OUTPUT;
    // echo '<p>' .($cm->name).'</p>';
    // Icon, module type and name.
    $modulename = get_string('modulename', $cm->modname);
    $icon = $OUTPUT->pix_icon('icon', $modulename, $cm->modname, array('class' => 'iconlarge activityicon'));
    $attributes = array();
    if (!$cm->visible) {
        $attributes['class'] = 'dimmed';
    }
    $text = html_writer::link("$CFG->wwwroot/mod/$cm->modname/view.php?id=$cm->id", format_string($cm->name), $attributes);
    $attributes = array('class' => 'sharedspaceModuleTitle');
    $moduletitle = HTML_WRITER::tag('div', $icon.$text, $attributes);
    echo $moduletitle;
}

function get_id_cat_lab() {
    global $DB;
    $cat = $DB->get_record('course_categories', ['name' => 'Lab']);
    return $cat->id;
}

/********************************************************/
/****************** Parse and Load **********************/
/********************************************************/

/**
 * 
 * Parse the .csv file given as argument $file.
 * Each line starts with an id of a course module, its type, its name and contains a serie of metadata to update
 * @param $filename: name of the file (not the path) as a STRING
 * @param $filecontent: text content of the file as a STRING
 */
function parse_and_load($filename, $filecontent) {
    global $DB;
    echo "FILE PARSED: " . $filename . "<br/> <br/>";
    
    //Hadrien: je ne comprends pas la gestion des sauts de lignes dans une chaîne de caractères en php...
    $lines = explode("\r", $filecontent); //create array separate by new line (assuming line separator is "\n")
    $lines_windows = explode("\n", $filecontent); //second attempt in case the separator is "\n"
    if (count($lines_windows) > count($lines)) {  //keep the maximum number of lines... (ouah c'est grace à ce stade...)
        $lines = $lines_windows;
    }


    $field_names = explode(';',$lines[0]); //Extract the names of the fields as an array
    echo "number of lines: " . count($lines) . "</br>";
    print_r($lines[0]);

    for ($j=1;$j<count($lines);$j++) {
        echo "PARSE LINE " . $j . " : " . $lines[$j] . "<br/>";
        $field_values     = explode(';',$lines[$j]); //Extract the values as an array
        $course_module_id = $field_values[0];
        $sql_module = "SELECT * FROM `course_modules` WHERE `id`='".$course_module_id."'";
        echo $sql_module . "<br/>";
        $instanceid = $DB->get_records_sql($sql_module);

        //code fabrice qui ne fonctionne pas:
        //$instanceid = $DB->get_field('course_modules','id',["id" =>$course_module_id]);
        
        if ($instanceid != NULL) {
            $cm = get_course_and_cm_from_cmid($instanceid[$course_module_id]);
            print_module($cm[1]);
            $shift = 3;
            for ($i = $shift; $i < count($field_values); $i++) {
                $fieldname_i = $field_names[$i];
                $value_i = $field_values[$i];
                echo htmltab() . " SET " . $field_names[$i] . " = " . $value_i . "<br/>";
                //1) get the id of the field from the name of the field 
                $field_id_sql = "SELECT * FROM `local_metadata_field` WHERE `name`='" . $fieldname_i . "'";
                $field_id = extractKeyFromDBResult($DB->get_records_sql($field_id_sql));
                echo big_htmltab() ." 'field name': " . $fieldname_i . " 'id': " . $field_id . " <br/>";               
                //TODO Had: what if field_id is unknown (== NULL) ?
                if ($field_id == NULL) {
                    echo big_htmltab() ." the 'field name' : " . $fieldname_i . " does not seem to be a valid name of an existing metadata <br/>";  
                } else {
                    //2) get the id of the local data entry (maybe learn how to do a join between two tables ?)
                    $metadata_id_sql = "SELECT `id` FROM `local_metadata` WHERE `instanceid`='" . $course_module_id . "' AND `fieldid`='" . $field_id. "'";
                    $metadata_id = extractKeyFromDBResult($DB->get_records_sql($metadata_id_sql));
                    if ($metadata_id == NULL) {
                        echo big_htmltab() . " 'id' (in metadata table) for module ". $course_module_id . " does not exist yet <br/>";
                    } else {
                        echo big_htmltab() . " 'id'(in metadata table): ". $metadata_id . "<br/>";
                    }
                    //3) Build the new record
                    $rewrecord             = new stdclass;
                    $rewrecord->data       = $value_i; 
                    $rewrecord->instanceid = $course_module_id;
                    $rewrecord->fieldid    = $field_id;  
                    //TODO: $rewrecord->dataformat = ?;  
                    if ($metadata_id == NULL) { //3) INSERT the new record
                        $recid = $DB->insert_record('local_metadata', $rewrecord, $returnid=true, $bulk=false);
                        echo big_htmltab() ." Param INSERT: ";
                        print_r($rewrecord);
                        echo " -- New 'id': " . $recid;
                        echo "<br/>";
                    } else { //3) UPDATE the existing record          
                        $rewrecord->id         = $metadata_id;
                        $success = $DB->update_record('local_metadata', $rewrecord, $bulk=false);
                        echo big_htmltab() ." Param UPDATE: ";
                        print_r($rewrecord );
                        echo " -- Success: " . $success;
                        echo "<br/>";
                    }
                }
            }
            echo "<br/>";
        } else {
            echo "Module id not found";
        }
        /*if ($instanceid != NULL) {
            $cm = get_course_and_cm_from_cmid($instanceid);
            print_module($cm[1]);
            $shift = 4; //index of the first column containing the metada
            for ($i = $shift; $i < count($field_values); $i++) {
                echo htmltab() . " SET " . $field_names[$i] . " = " . $field_values[$i] . "<br/>";
                //1) get the id of the field from the name of the field 
                //$field_id = $DB->get_field("local_metadata_field", "id", ["name" => $field_names[$i]]);
                $field_id = extractKeyFromDBResult($DB->get_records_sql($field_id_sql));
                echo big_htmltab() ." 'field name': " . $field_names[$i] . " 'id': " . $field_id . " <br/>";               
                //TODO Had: what if field_id is unknown (== NULL) ?
                
                //2) get the id of the local data entry (maybe learn how to do a join between two tables ?)
                //$metadata_id = $DB->get_field("local_metadata", "id", ["instanceid" => $course_module_id, "fieldid" => $field_id]);
                $metadata_id = extractKeyFromDBResult($DB->get_records_sql($metadata_id_sql));
                echo big_htmltab() . " 'id'(in metadata table): ". $metadata_id . "<br/>";
                
                //3) Build the new record
                $rewrecord             = new stdclass;
                $rewrecord->data       = $field_values[$i]; 
                $rewrecord->instanceid = $course_module_id;
                $rewrecord->fieldid    = $field_id;  
                //TODO: $rewrecord->dataformat = ?;  
                if ($metadata_id == NULL) { //3) INSERT the new record
                    $recid = $DB->insert_record('local_metadata', $rewrecord, $returnid=true, $bulk=false);
                    echo big_htmltab() ." Param INSERT: ";
                    print_r($rewrecord);
                    echo " -- New 'id': " . $recid;
                    echo "<br/>";
                } else { //3) UPDATE the existing record          
                    $rewrecord->id         = $metadata_id;
                    $success = $DB->update_record('local_metadata', $rewrecord, $bulk=false);
                    echo big_htmltab() ." Param UPDATE: ";
                    print_r($rewrecord );
                    echo " -- Success: " . $success;
                    echo "<br/>";
                }
            }
            echo "<br/>";
        } else {
            echo "Module id not found";
        }*/
    }
    
}

/**
 * $result_from_db is something like an array of stdClassObject:
 * Array ( [1] => stdClass Object ( [id] => 1 [contextlevel] => 70 [shortname] => Creator))
 */
function extractKeyFromDBResult($result_from_db) {
    if (count($result_from_db) > 1) {
        echo "Error from database query, we got more than one entry <br/>";
        print_r($result_from_db);
    } else { //HOW TO GET THE FIRST ENTRY OF AN ASSOCIATIVE ARRAY ???
        //hadrien: do an array_keys directly ?
        //foreach ($result_from_db as $key => $value) {
        //    return $key;   
        //}
        return array_keys($result_from_db)[0];
    }
}


/**
 * Hadrien coding in php (not proud...) :
 */
function htmltab() {
    return " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
}

/**
 * Hadrien coding in php again (please stop !) :
 */
function big_htmltab() {
    return " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
}


/********************************************************/
/****************** write and download ******************/
/********************************************************/

/**
 * Truncate the string at a given length but does not
 * cut a word in the middle so the string can be a bit longer (of a word's size) than
 * the length given as limit
 */
function truncate($string,$length=100,$append=" ... ") {
    $string = rtrim($string);

    if(strlen($string) > $length) {
        $string = wordwrap($string, $length);
        $string = explode("\n", $string, 2);
        $string = $string[0] . $append;
    }

    return $string;
}

/**
 * concatenate all the metadata fields name in a csv format.
 * This is the header of .csv file containing the metadata as columns.
 * @param $header: a string
*/
function appendMetaDataName($header) {
    global $DB;
    $fields = array_keys($DB->get_records_sql("SELECT `name`  FROM `local_metadata_field`"));
    foreach ($fields as $field) {
        $header = $header . $field . ";";
    }
    return $header;
}

/**
 * Append a new line corresponding to the id, type and name of the module in the string given as argument
 * @param $file_content_string: a file content as a STRING
 * @param $module_id: the id of a module in table course_modules as an INTEGER 
*/
function appendModuleInfo($file_content_string, $module_id) {
    global $DB;
    $modules_type_id  = extractKeyFromDBResult($DB->get_records_sql("SELECT `module` FROM `course_modules` WHERE `id`='" . $module_id. "'"));
    $module_type_name = extractKeyFromDBResult($DB->get_records_sql("SELECT `name` FROM `modules` WHERE `id`='" . $modules_type_id. "'"));
    
    //Add activity type and
    $file_content_string = $file_content_string . $module_id . ';' . $module_type_name . ';';

    $module_instance         = extractKeyFromDBResult($DB->get_records_sql("SELECT `instance` FROM `course_modules` WHERE `id`='" . $module_id. "'"));
    $module_named_by_user = extractKeyFromDBResult($DB->get_records_sql("SELECT `name` FROM `".$module_type_name."` WHERE `id`='" . $module_instance . "'"));
    if ($module_type_name == 'label') {
        $file_content_string = $file_content_string . " label's name ignored ";
    } else {
        $file_content_string = $file_content_string . truncate($module_named_by_user, 50, " ... ");
    }
    $file_content_string = $file_content_string . PHP_EOL;
    return $file_content_string;
}

/**
 * Export as a .csv file all the modules of a given course.
 * @param $course_name: the name of the course
 * @param $userid: the id of the user requesting the export 
 */
function writeModulesOfCourse($course_name, $userid) {

    global $DB, $CFG;
    echo "Modules extraction of " . $course_name . "<br/>";
    $course_id = extractKeyFromDBResult($DB->get_records_sql("SELECT * FROM `course` WHERE `fullname`='" . $course_name . "'"));
    $modules_id    = array_keys($DB->get_records_sql("SELECT `id` FROM `course_modules` WHERE `course`='" . $course_id . "'"));
    
    echo htmltab()."Number of modules found " . count($modules_id) . "<br/>";
    //print_r($modules_id);
    
    //1)Create the file CONTENT
    $file_content_string = appendMetaDataName('id;type;name;') . PHP_EOL;
    for ($i = 0; $i < count($modules_id); $i++) {    
        $file_content_string = appendModuleInfo($file_content_string, $modules_id[$i]);
    }

    //2) Turn the content into a file to download (SKIP MOODLE FILE MANAGEMENT)
    $name_file = 'modules_course_' . $course_id . '_'. $userid . '.csv';
    $path_file = '/var/www/moodle/local/sharedspace/uploads/' . $name_file;//$CFG->wwwroot.'/local/sharedspace/uploads/' . $name_file;
    $file_itself = fopen($path_file, 'a+');
    file_put_contents($path_file, $file_content_string);
    echo '<a href="'.$CFG->wwwroot.'/local/sharedspace/uploads/' . $name_file . '">' . $name_file . '</a><br/>';
    fclose($file_itself);
}

/**
 * Export as a .csv file all the modules of a given course.
 * @param int $courseid: the name of the course
 * @param int $userid: the id of the user requesting the export
 */
// function writeModulesOfCourse($courseid, $userid) {
//     global $DB, $CFG;
//     require_once($CFG->libdir . '/csvlib.class.php');
//     $modinfo = get_fast_modinfo($courseid, -1);
//     $name_file = 'modules_course_' . $courseid . '_'. $userid . '.csv';
//     $csvexport = new csv_export_writer($this->separator);
//     $csvexport->set_filename($name_file);
//     // 1)Create the file CONTENT
//     $data = $DB->get_fieldset_select('local_metadata_field', 'name');
//     array_shift($data, 'id', 'type', 'name');
//     $csvexport->add_data($data);
//     foreach ($modinfo->instances as $module => $instances) {
//         foreach ($instances as $index => $cm) {
//                 $data = [ $cm->id,
//                           $module,
//                           $cm->name
//                         ];
//                 $csvexport->add_data($data);
//         }
//     }
//     // echo htmltab() . " Content of the file <br/>";
//     // echo htmltab() . " " .$csvexport->print_csv_data . "<br/>";
//     $csvexport->download_file();

// }