
<?php

defined('MOODLE_INTERNAL') || die;

$capabilities = array(
    'local/sharedspace:accesstospace' => array(
        'riskbitmask' => RISK_SPAM,
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'coursecreator' => CAP_ALLOW
        )
    ),
 );