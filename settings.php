<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $ADMIN->add('localplugins', new admin_category('sharedspacefolder', get_string('sharedspace', 'local_sharedspace')));
    $contextplugins = core_component::get_plugin_list('sharedspacecontext');

    // Create a settings page and add an enable setting for each metadata context type.
    //
    $settings = new admin_settingpage('local_sharedspace', 'Caseine Shared Space');
    $settings->add(new admin_setting_configtext('local_sharedspace/option', 'Caseine Shared Space Option', 'Information about this option', 100, PARAM_INT));
    //$settings->add(new admin_setting_configtext('local_thisplugin/option', get_string('introzozo','local_sharedspace'), 'Aaaah ok', 100, PA));
    $settings->add(new admin_setting_configcheckbox('local_sharedspace', get_string('introzozo','local_sharedspace'),'Aaaaah', 0));
    //
    //$settings = new admin_settingpage('local_sharedspace', get_string('settings');
    //$settings->add(new admin_setting_configtext('local_thisplugin/option', 'Caseine Shared Space Option', 'Information about this option', 100, PARAM_INT));
    // $item = new admin_setting_configcheckbox('Shared Caseine Space is not for the zozos', 'Yo man','', 0);
    /*if ($ADMIN->fulltree) {
        foreach ($contextplugins as $contextname => $contextlocation) {
            $item = new admin_setting_configcheckbox('metadatacontext_'.$contextname.'/metadataenabled',
                new lang_string('metadataenabled', 'metadatacontext_'.$contextname), '', 0);
            $settings->add($item);
        }
    }*/
    $ADMIN->add('sharedspacefolder', $settings);


    $settings = null;
}
