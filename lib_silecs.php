<?php

# M2.8
function local_extsearch_extends_navigation(global_navigation $nav) {
    global $PAGE;
    static $jsInserted = false;
    $nav->add(get_string('linkname', 'local_extsearch'), new moodle_url('/local/extsearch/index.php'));
    if (!$jsInserted) {
        $PAGE->requires->js_init_code('
window.toggleDetails = function (n) {
    if (!/\btoggle-details\b/.exec(n.className)) {
        return true;
    }
    var details = n.getElementsByTagName("div")[0];
    var disp = details.style.display;
    details.style.display = (disp == "block" ? "none" : "block");
    return false;
}
');
        $jsInserted = true;
    }
}

# M2.9
function local_extsearch_extend_navigation(global_navigation $nav) {
    global $PAGE;
    static $jsInserted = false;
    $nav->add(get_string('linkname', 'local_extsearch'), new moodle_url('/local/extsearch/index.php'));
    if (!$jsInserted) {
        $PAGE->requires->js_init_code('
window.toggleDetails = function (n) {
    if (!/\btoggle-details\b/.exec(n.className)) {
        return true;
    }
    var details = n.getElementsByTagName("div")[0];
    var disp = details.style.display;
    details.style.display = (disp == "block" ? "none" : "block");
    return false;
}
');
        $jsInserted = true;
    }
}

/**
 * A list of courses that match a search
 *
 * @global object $CFG
 * @global object $DB
 * @param array|object $criteria An assoc array of search terms, or an object containing a QuickForm input
 * @param string $sort A field and direction to sort by
 * @param int $page The page number to get
 * @param int $recordsperpage The number of records per page
 * @param int $totalcount Passed in by reference.
 * @return object {@link $COURSE} records
 */
function get_courses_extsearch($criteria, $sort='fullname ASC', $page=0, $recordsperpage=50, &$totalcount) {
    global $CFG, $DB;

    if ($DB->sql_regex_supported()) {
        $REGEXP    = $DB->sql_regex(true);
        $NOTREGEXP = $DB->sql_regex(false);
    }

    $searchcond = array();
    $params     = array();
    $i = 0;

    // Thanks Oracle for your non-ansi concat and type limits in coalesce. MDL-29912
    if ($DB->get_dbfamily() == 'oracle') {
        $concat = "(c.summary|| ' ' || c.fullname || ' ' || c.idnumber || ' ' || c.shortname)";
    } else {
        $concat = $DB->sql_concat("COALESCE(c.summary, '')", "' '", 'c.fullname', "' '", 'c.idnumber', "' '", 'c.shortname');
    }

    if (is_array($criteria)) {
        $search = new StdClass();
        $searchterms = $criteria;
    } else {
        $search = trim(strip_tags($criteria->search)); // trim & clean raw searched string
        $searchterms = array();
        if ($search) {
            $searchterms = explode(" ", $search);    // Search for words independently
            foreach ($searchterms as $key => $searchterm) {
                if (strlen($searchterm) < 2) {
                    unset($searchterms[$key]);
                }
            }
        }
    }

    foreach ($searchterms as $searchterm) {
        $i++;

        $NOT = false; /// Initially we aren't going to perform NOT LIKE searches, only MSSQL and Oracle
                   /// will use it to simulate the "-" operator with LIKE clause

    /// Under Oracle and MSSQL, trim the + and - operators and perform
    /// simpler LIKE (or NOT LIKE) queries
        if (!$DB->sql_regex_supported()) {
            if (substr($searchterm, 0, 1) == '-') {
                $NOT = true;
            }
            $searchterm = trim($searchterm, '+-');
        }

        // TODO: +- may not work for non latin languages

        if (substr($searchterm,0,1) == '+') {
            $searchterm = trim($searchterm, '+-');
            $searchterm = preg_quote($searchterm, '|');
            $searchcond[] = "$concat $REGEXP :ss$i";
            $params['ss'.$i] = "(^|[^a-zA-Z0-9])$searchterm([^a-zA-Z0-9]|$)";

        } else if (substr($searchterm,0,1) == "-") {
            $searchterm = trim($searchterm, '+-');
            $searchterm = preg_quote($searchterm, '|');
            $searchcond[] = "$concat $NOTREGEXP :ss$i";
            $params['ss'.$i] = "(^|[^a-zA-Z0-9])$searchterm([^a-zA-Z0-9]|$)";

        } else {
            $searchcond[] = $DB->sql_like($concat,":ss$i", false, true, $NOT);
            $params['ss'.$i] = "%$searchterm%";
        }
    }

    // other course settings
    if (property_exists($criteria, 'visible')) {
        $searchcond[] = "c.visible >= " . ((int) $criteria->visible);
    }
    if (!empty($criteria->startdateafter)) {
        $searchcond[] = "c.startdate >= " . ((int) $criteria->startdateafter);
    }
    if (!empty($criteria->startdatebefore)) {
        $searchcond[] = "c.startdate <= " . ((int) $criteria->startdatebefore);
    }
    if (!empty($criteria->createdafter)) {
        $searchcond[] = "c.timecreated >= " . ((int) $criteria->createdafter);
    }
    if (!empty($criteria->createdbefore)) {
        $searchcond[] = "c.timecreated <= " . ((int) $criteria->createdbefore);
    }

    // custominfo fields
    $fields = $DB->get_records('custom_info_field', array('objectname' => 'course'));
    $searchjoin = array();
    if ($fields) {
        $i = 0;
        foreach ($fields as $field) {
            $formfield = custominfo_field_factory('course', $field->datatype, $field->id, null);
            if (!empty($criteria->{$formfield->inputname})) {
                $i++;
                $formfield->edit_data($criteria);
                if ($formfield->data) {
                    $searchjoin[] = "JOIN {custom_info_data} d$i "
                        ."ON (d$i.objectid = c.id AND d$i.objectname='course' AND d$i.fieldid={$field->id})";
                    $searchcond[] = $DB->sql_like("d$i.data", ":dd$i", false, true, false);
                    $params['dd'.$i] = "%{$formfield->data}%";
                }
            }
        }
    }

    // category
    if (!empty($criteria->category)) {
        $searchcond[] = "c.category = :categoryid";
        $params['categoryid'] = (int) $criteria->category;
    }

    if (empty($searchcond)) {
        if (get_config('local_extsearch', 'acceptemptycriteria')) {
            $searchcond = array('1=1');
        } else {
            return array();
        }
    }

    $searchjoin = implode(" ", $searchjoin);
    $searchcond = implode(" AND ", $searchcond);

    $courses = array();
    $c = 0; // counts how many visible courses we've seen

    // Tiki pagination
    $limitfrom = $page * $recordsperpage;
    $limitto   = $limitfrom + $recordsperpage;

    $ctxselect = context_helper::get_preload_record_columns_sql('ctx');
    $params['contextlevel'] = CONTEXT_COURSE;

    $sql = "SELECT c.*, $ctxselect
              FROM {course} c
                   $searchjoin
                   JOIN {context} ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = :contextlevel)
             WHERE $searchcond AND c.id <> " . SITEID . "
          ORDER BY $sort";

    $rs = $DB->get_recordset_sql($sql, $params);
    foreach($rs as $course) {
        // preload contexts only for hidden courses or courses we need to return
        context_helper::preload_from_record($course);
        $course->context = context_course::instance($course->id);
        if ($course->visible || has_capability('moodle/course:viewhiddencourses', $course->context)) {
            // Don't exit this loop till the end
            // we need to count all the visible courses
            // to update $totalcount
            if ($c >= $limitfrom && $c < $limitto) {
                $courses[$course->id] = $course;
            }
            $c++;
        }
    }
    $rs->close();

    // our caller expects 2 bits of data - our return
    // array, and an updated $totalcount
    $totalcount = $c;
    return $courses;
}

/**
 * Print a description of a course, suitable for browsing in a list.
 *
 * @param object $course the course object.
 * @param string $highlightterms (optional) some search terms that should be highlighted in the display.
 */
function extsearch_print_course($course, $highlightterms = '') {
    global $CFG, $USER, $DB, $OUTPUT;

    $context = context_course::instance($course->id);

    // Rewrite file URLs so that they are correct
    $course->summary = file_rewrite_pluginfile_urls($course->summary, 'pluginfile.php', $context->id, 'course', 'summary', NULL);

    echo html_writer::start_tag('div', array('class'=>'coursebox clearfix'));
    echo html_writer::start_tag('div', array('class'=>'info'));
    echo html_writer::start_tag('h3', array('class'=>'name'));

    $linkhref = new moodle_url('/course/view.php', array('id'=>$course->id));

    $coursename = get_course_display_name_for_list($course);
    $linktext = highlight($highlightterms, format_string($coursename));
    $linkparams = array('title'=>get_string('entercourse'));
    if (empty($course->visible)) {
        $linkparams['class'] = 'dimmed';
    }
    echo html_writer::link($linkhref, $linktext, $linkparams);
    echo html_writer::end_tag('h3');

    /// first find all roles that are supposed to be displayed
    if (!empty($CFG->coursecontact)) {
        $managerroles = explode(',', $CFG->coursecontact);
        $rusers = array();

        if (!isset($course->managers)) {
            list($sort, $sortparams) = users_order_by_sql('u');
            $rusers = get_role_users($managerroles, $context, true,
                'ra.id AS raid, u.id, u.username, u.firstname, u.lastname,
                 u.firstnamephonetic, u.lastnamephonetic, u.middlename, u.alternatename,
                 rn.name AS rolecoursealias,
                 r.name AS rolename, r.sortorder, r.id AS roleid, r.shortname AS roleshortname',
                'r.sortorder ASC, ' . $sort, null, '', '', '', '', $sortparams);
        } else {
            //  use the managers array if we have it for perf reasosn
            //  populate the datastructure like output of get_role_users();
            foreach ($course->managers as $manager) {
                $user = clone($manager->user);
                $user->roleid = $manager->roleid;
                $user->rolename = $manager->rolename;
                $user->roleshortname = $manager->roleshortname;
                $user->rolecoursealias = $manager->rolecoursealias;
                $rusers[$user->id] = $user;
            }
        }

        $namesarray = array();
        $canviewfullnames = has_capability('moodle/site:viewfullnames', $context);
        foreach ($rusers as $ra) {
            if (isset($namesarray[$ra->id])) {
                //  only display a user once with the higest sortorder role
                continue;
            }

            $role = new stdClass();
            $role->id = $ra->roleid;
            $role->name = $ra->rolename;
            $role->shortname = $ra->roleshortname;
            $role->coursealias = $ra->rolecoursealias;
            $rolename = role_get_name($role, $context, ROLENAME_ALIAS);

            $fullname = fullname($ra, $canviewfullnames);
            $namesarray[$ra->id] = $rolename.': '.
                html_writer::link(new moodle_url('/user/view.php', array('id'=>$ra->id, 'course'=>SITEID)), $fullname);
        }

        if (!empty($namesarray)) {
            echo html_writer::start_tag('ul', array('class'=>'teachers'));
            foreach ($namesarray as $name) {
                echo html_writer::tag('li', $name);
            }
            echo html_writer::end_tag('ul');
        }
    }
    echo html_writer::end_tag('div'); // End of info div

    echo html_writer::start_tag('div', array('class'=>'summary'));
    $options = new stdClass();
    $options->noclean = true;
    $options->para = false;
    $options->overflowdiv = true;
    if (!isset($course->summaryformat)) {
        $course->summaryformat = FORMAT_MOODLE;
    }
    echo highlight($highlightterms, format_text($course->summary, $course->summaryformat, $options,  $course->id));
    if ($icons = enrol_get_course_info_icons($course)) {
        echo html_writer::start_tag('div', array('class'=>'enrolmenticons'));
        foreach ($icons as $icon) {
            $icon->attributes["alt"] .= ": ". format_string($coursename, true, array('context'=>$context));
            echo $OUTPUT->render($icon);
        }
        echo html_writer::end_tag('div'); // End of enrolmenticons div
    }

    // add a toggable block
    echo html_writer::start_tag('div', array('class'=>'toggle-details', 'onclick' => 'toggleDetails(this)'));
    echo "<a>" . get_string('detaileddescription', 'local_extsearch') . "&hellip;</a>";
    echo html_writer::start_tag('div', array('style' => 'display: none'));
    $fieldsTree = json_decode(get_config('local_extsearch', 'descrfieldsconfig'), true);
    $categories = custominfo_get_categories(array_keys($fieldsTree));
    if ($categories) {
        foreach ($categories as $category) {
            $fields = custominfo_get_fields($fieldsTree, $category);
            if ($fields) {
                echo $OUTPUT->heading($category->name, 4);
                $table = new html_table();
                foreach ($fields as $field) {
                    $row =  new html_table_row();
                    // th
                    $th = new html_table_cell($field->name);
                    $th->header = true;
                    $th->attributes = array('class' => 'attr-' . $field->shortname);
                    $row->cells[] = $th;
                    // td
                    $formfield = custominfo_field_factory('course', $field->datatype, $field->id, $course->id);
                    $row->cells[] = new html_table_cell($formfield->display_data());
                    // add the row to the table
                    $table->data[] = $row;
                }
                echo html_writer::table($table);
            }
        }
    }
    echo html_writer::end_tag('div');
    echo html_writer::end_tag('div');
    // end of toggable block

    echo html_writer::end_tag('div'); // End of summary div
    echo html_writer::end_tag('div'); // End of coursebox div
}

/**
 * Returns the course metadata categories (as DB records) that match the given list of names.
 *
 * @global moodle_database $DB
 * @param array $names
 * @return array
 */
function custominfo_get_categories($names) {
    global $DB;
    list ($sqlin, $sqlparams) = $DB->get_in_or_equal($names);
    if ($sqlin) {
        $categories = $DB->get_records_select(
                'custom_info_category', "objectname = 'course' AND name " . $sqlin, $sqlparams, 'sortorder ASC'
        );
    } else {
        $categories = array();
    }
    return $categories;
}

/**
 * Returns all the fields (as DB records) that match the given $category in the fields tree.
 *
 * @global moodle_database $DB
 * @param array $fieldsTree
 * @param stdClass $category
 * @return array
 */
function custominfo_get_fields($fieldsTree, $category) {
    global $DB;
    $fields = array();
    if (!empty($fieldsTree[$category->name])) {
        if (!empty($fieldsTree[$category->name])) {
            list ($sqlin, $sqlparams) = $DB->get_in_or_equal($fieldsTree[$category->name]);
            if ($sqlin) {
                $sqlparams[] = $category->id;
                $fields = $DB->get_records_select(
                        'custom_info_field', "shortname $sqlin AND categoryid = ?", $sqlparams, 'sortorder ASC'
                );
            }
        }
    } else if (isset($fieldsTree[$category->name])) {
        // every field in that category
        $fields = $DB->get_records('custom_info_field', array('categoryid' => $category->id), 'sortorder ASC');
    }
    return $fields;
}
