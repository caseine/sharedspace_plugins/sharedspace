

<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package shared_space
 * @author Hadrien Cambazard, Nicolas Catusse
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2017 onwards Les zozos de chez caseine
 */
$string['pluginname'] = 'Shared Caseine Space';

$string['errorcontextnotfound'] = 'Invalid context subplugin "{$a->contextname}" requested.';
$string['sharedspace'] = 'Shared Caseine Space';
$string['sharedspacefor'] = 'Instance sharedspace';
$string['sharedspacesaved'] = 'Sharedspace saved.';
$string['subplugintype_sharedspacecontext'] = 'Space context';
$string['subplugintype_sharedspacecontext_plural'] = 'Space contexts';
$string['introzozo'] = 'Shared Caseine Space Zozos Allowed ?';
$string['sharedspace:accesstospace'] = 'Capability to access the shared space';
$string['error_when_accessing'] = 'This space is dedicated to teachers. If you are a teacher then you are not yet part of the caseine community to access all shared contents. <br/> Please contact the caseine team.';
$string['nocoursefound'] = 'No course matches these criteria.';