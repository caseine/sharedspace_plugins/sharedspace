<?php

require('../../config.php');

global $CFG,$PAGE;

require_once($CFG->libdir.'/adminlib.php');
//require_once('lib.php');
require_once($CFG->dirroot.'/local/sharedspace/classes/spaceform.php');
require_once($CFG->dirroot.'/local/sharedspace/lib.php');

//$edit   = optional_param('edit', null, PARAM_BOOL);    // Turn editing on and off

require_login();

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url($CFG->wwwroot.'/local/sharedspace/index.php'));
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('sharedspace', 'local_sharedspace'));
//$PAGE->blocks->load_blocks();
$PAGE->set_blocks_editing_capability('moodle/my:manageblocks');
$PAGE->blocks->add_region('content');

/*if($PAGE->user_allowed_editing()) {
    // Add button for editing page
    $params = array('edit' => !$edit);
}*/

echo $OUTPUT->header();
echo $OUTPUT->custom_block_region('content');
//Instantiate simplehtml_form 
//require_login();
$mform = new spaceform();

//<hadrien> Extract context and capabilities ************
$systemcontext = context_system::instance();
$userid = $USER->id; //<hadrien> user identifier 0 if anonymous user
//$allowed_in_space = has_capability('local/sharedspace:accesstospace', $systemcontext);


//print(get_string('sharedspace', 'local_sharedspace'));

/*if (!$allowed_in_space) {
    throw new required_capability_exception($systemcontext, 'local/sharedspace:accesstospace', 'error_when_accessing', 'local_sharedspace');
}*/
//$usercontext = context_user::instance($userid, MUST_EXIST);
//************


require_capability('local/sharedspace:accesstospace',
                   $systemcontext, 
                   null,
                   true,
                   'error_when_accessing',
                   'local_sharedspace');


/*if (!$allowed_in_space) {
    echo '<p style="text-align: center"> You are not part of the caseine community to access all shared contents. <br/> If you want to join, please contact our coconut adviser </p>' ;
} else {
    
    //<hadrien> TEST ************
    print("Hadrien: START print context information to understand roles and capabilities");
    echo '<br/>';
    print("Are you allowed in space: " .$allowed_in_space);
    echo '<br/>';
    print("USER ID: ");
    print_r($userid);
    print(" FIN");
    echo '<br/>';
    print_r($systemcontext);
    echo '<br/>';
    //print_r($usercontext);
    //echo '<br/>';
    print("Hadrien: FIN ");echo '<br/>';
    // *************************
    */
    
print("The caseine shared space is under developpement, it is currently NOT fonctional.");
echo '<br/>';
    
    //Form processing and displaying is done here
    if ($mform->is_cancelled()) {
        //Handle form cancel operation, if cancel button is present on form
    } else if ($fromform = $mform->get_data()) {
    
        //print_r($fromform);
        //hadrien: ok get_object_vars convert an object into an array of its attributes values, jesus ...
        $buttonused = $fromform->submitbutton;//get_object_vars($fromform)['submitbutton'];
        echo $OUTPUT->box_start();
        
        if ($buttonused == 'Parse and load') {
            echo $OUTPUT->heading('Parsing and marking summary');
            $content_file = $mform->get_file_content('csv_auto_tag_file');
            parse_and_load($fromform, $content_file);
             
        } elseif ($buttonused == 'Download from course') {
            
            $menu_idx_selected = $fromform->course_selection;
            echo "Menu idx selected: " . $menu_idx_selected . "<br/>";
            $data_courses = array_keys($DB->get_records_sql("SELECT DISTINCT `fullname` FROM `course`")); //hadrien: debile de refaire la meme requete...
            $course_name_selected = $data_courses[$menu_idx_selected];
            echo "Course selected: " . $course_name_selected . "<br/>";
            writeModulesOfCourse($course_name_selected, $userid);

        } elseif ($buttonused == 'Download from category') {
            //TODO
        } else {
            $results = simple_request($fromform);

            echo $OUTPUT->heading('Result');
            if (empty($results)) {
                echo '<p style="text-align: center">' . get_string('nocoursefound', 'local_sharedspace') . "</p>";
                /*get_autocomplete_data($fromform);
                    foreach($fromform as $key => $value) {
                        print $key." ";
                        if (is_array($value)) {
                          print_r($value);
                        }
                        else {
                          print "$value";
                        }
                        print "\n";
                        //print "$key => $value\n";
                    }*/
            } else {
                echo "<ol>";
                foreach ($results as $cm) {
                  /*if ($course->visible != 1
                      && !has_capability('moodle/course:viewhiddencourses',$course->context)) {
                      continue;
                  }*/
                    print_module($cm);
                }
                echo "</ol>";
            }
        }
        echo $OUTPUT->box_end();
        //In this case you process validated data. $mform->get_data() returns data posted in form.
    } else {
        // this branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
        // or on the first display of the form.

        //Set default data (if any)
        //$mform->set_data($toform);
        //displays the form
        $mform->display();
    }
//}

echo $OUTPUT->footer();


